---
templateKey: slider-posts
title: Title
subtitle: Windows. For people.
description: Windows are an important part of any home. They let light in, and help to keep the temperature inside comfortable. While they help to improve the look of your property, it can be difficult to know when it’s time to replace them.
link: test
featuredImage: /img/image-1.png
---
