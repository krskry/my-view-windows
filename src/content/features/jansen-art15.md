---
title: "Jansen Art15 "
templateKey: features
description: >-
  ### Slim steel profile system for interior doors and walls


  The extremely reduced face width of Jansen Art'15, the new non-insulated steel profile system from Jansen, allows a unique design, while the steel material ensures a long service life. The extremely narrow, highly resilient steel profile system always ensures durable, highly stable constructions in an unparalleled slimline frame.


  **Benefits**


  * Minimal face widths

  * Transparent room divider in the interior

  * Three design options
featuredImage: /img/photo-work-2.jpeg
---
