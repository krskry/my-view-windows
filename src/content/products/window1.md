---
templateKey: product-post
title: Facade TP 52
ProductName: Fasad TP 52
date: 2020-12-26T17:15:10.102Z
description: "Lorem Ipsum is simply dummy text of the printing and typesetting
  industry. Lorem Ipsum has been the industry's standard dummy text ever since
  the 1500s, when an unknown printer took a galley of type and scrambled it "
featuredImage: /img/1500304973.368x342.fm.fe.0410.jpg
galleryImages:
  - /img/1500304979.368x342.fm.fe.0410.jpg
  - /img/1500304973.368x342.fm.fe.0410.jpg
  - /img/1500304977.368x342.fm.fe.0410z.jpg
category: Aluminium Windows
---
## **Supplier: Cortizo**

* Great variety of mullions (from 16 to 230 mm) and transoms (from 22,5 to 205,5) that solve the different aesthetic and constructive needs of any architectonic projects
* Internal seen section: 52 mm
* Extensive profile range, their mechanical unions allows for all types of façades to be built (vertically, inclined, 90° corners, corners, polygonal) as well as solving large and heavy glazing modulations
* Water tightness elements: drainage pipettes, tear strip gaskets and vulcanized angles
* In order to fix the glass to the supporting profiles, a pressed profile is needed, this profile is screwed to the mullions and transoms, and covered with a embellisher profile (cover profile)

* Glazing

Maximum glazing 64 mm

Mínimum glazing 4 mm

Consult us for greater thicknesses

* Opening possibilities

 Hidden turn&tilt

 Hidden side hung

 hidden top hung projecting

* Sections

 Mullion 52 mm


* Profile thickness

Mullion 2,1 y 3 mm

Transom 2,1 mm


* Polyamide strip length

## 6, 12 & 30 mm stackable profiles

* Minimum / Maximum opening dimensions\

  Hidden Top hung

Min. Width (L) = 500 mm

 Min. Height (H) = 650 mm

 Max. Width (L) = 2500 mm

 Max. Height (H) = 2500 mm

 
## Hidden Parallel Projecting

 Min. Width (L) = 530 mm

  Min. Height (H) = 530 mm

  Max. Width (L) = 2000 mm

  Min. Height (H) = 3000 mm


 ## Hidden Side Hung / Turn&Tilt

Min. Width (L) = 500 mm

Min. Height (H) = 600 mm

Max. Width (L) = 1400 mm

Min. Height (H) = 1900 mm

 